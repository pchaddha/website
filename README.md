Resume Website
=====

Meta
---
- Author: `Parmandeep Chaddha`
- Date Created: `Jan 11 2021`
- Website URL: `https://parmanchaddha.web.app`

Purpose
---
Create an online resume because silicon is cooler than paper.

Sourcode
---
- The entire repository is public. If the application is public, why shouldn't the source code be?
- Repository URL: `https://bitbucket.org/pchaddha/website/src/master/`

Contribution
---
- I don't know why anyone would want to contribute to my personal website. But if you want to, send an email to `parmandeepchaddha@gmail.com` and I can set you up with a user and password.