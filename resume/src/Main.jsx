import React from "react";

import './assets/App.css';
import ResumeTheme from "./assets/ThemeProvider";

import Navbar from "./pages/navbar/Navbar";
import Home from "./pages/home/Home";
import Skills from "./pages/skills/Skills";
import Experience from "./pages/experience/Experience";
import Projects from "./pages/projects/Projects";
import Education from "./pages/education/Education";
import ContactMe from "./pages/contactMe/ContactMe";
import PDFResume from "./pages/pdfResume/PDFResume";

class Main extends React.Component {
  render( ) {
    return (
      <div className="app">
        <ResumeTheme>
            <Navbar/>
            <Home />
            <Skills/>
            <Experience/>
            <Projects/>
            <Education/>
            <ContactMe/>
            <PDFResume/>
        </ResumeTheme>
      </div>
    )
  }
}

export default Main
