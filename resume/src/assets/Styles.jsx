import { makeStyles } from "@material-ui/core/styles";

export const useStyle = makeStyles((theme) => ({
    navbarContainer: {
        position: "sticky",
        width: '100vw',
        top: 0,
        backgroundColor: "white",
        paddingTop: 10,
        paddingBottom: 10,
        zIndex: 1000,
    },
    navbar: {
        textAlign: "center",
        [theme.breakpoints.up('md')]: {
            width: "50%",
            display: "block",
            margin: "10px auto",
        },
        [theme.breakpoints.down('sm')]: {
            display: "block",
        },
    },
    resumeTitle: {
        paddingTop: theme.spacing(1),
        fontWeight: "100",
        textAlign: "center",
        [theme.breakpoints.down('sm')]: {
            fontSize: '2.5em',
        },
    },
    title: {
        paddingTop: theme.spacing(1),
        fontWeight: "100",
        [theme.breakpoints.up('md')]: {
            textAlign: "center",
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '2.0em',
        },
    },
    contactMeButton: {
        borderRadius: "20px",
        [theme.breakpoints.up('md')]: {
            margin: "5px 20%",
            width: "60%"        
        },
        [theme.breakpoints.down('sm')]: {
            width: '90%',
            fontSize: '0.8em'
        },
    },
    aboutMePaper: {
        [theme.breakpoints.up('md')]: {
            width: "50%",
            margin: "2vh 25%"
        },
        [theme.breakpoints.down('sm')]: {
            width: '100%',
            margin: '2vh 10%',
        },
    },
}));