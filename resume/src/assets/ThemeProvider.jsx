import React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { CssBaseline } from "@material-ui/core"
import { ThemeProvider } from '@material-ui/styles';

export const SITE_COLORS = {
  purple: '#4e148c',
  grey: '#6c6f7d',
  charcoal: '#2e3138',
};

const theme = createMuiTheme({
  palette: {
    primary: {
      main: SITE_COLORS.purple,
    },
    secondary: {
      main: SITE_COLORS.grey,
    },
    text: {
      primary: SITE_COLORS.charcoal,
      secondary: SITE_COLORS.purple,
    },
  },
  typography: {
    fontFamily: [
      'Poppins',
    ].join(','),
    fontWeight: 400,
  },
  overrides: {
    MuiCssBaseline: {
      "@global": {
        body: {backgroundImage: "url(/static/skills/background.png)"}
      }
    }
  }
});

export default function ResumeTheme(props) {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {props.children}
    </ThemeProvider>
  );
}
