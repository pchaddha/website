import React from "react";

import {useStyle} from "../../assets/Styles";

import {PageTitle, SectionDescription, SectionPaper} from "../../utilities/Components";

import EmailRoundedIcon from '@material-ui/icons/EmailRounded';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import GitHubIcon from '@material-ui/icons/GitHub';


import {Button} from "@material-ui/core";

const contactInfo = [
    {
        type: "email",
        value: "parmandeepchaddha@gmail.com",
        link: "mailto:parmandeepchaddha@gmail.com",
        icon: <EmailRoundedIcon/>
    },
    {
        type: "LinkedIn",
        value: "LinkedIn",
        link: "https://www.linkedin.com/in/parman-chaddha/",
        icon: <LinkedInIcon/>,
    },
    {
        type: "git",
        value: "BitBucket",
        link: "https://bitbucket.org/pchaddha/",
        icon: <GitHubIcon />,
    },
];

export default function ContactMe() {
    const classes = useStyle();

    return (
        <div className="page">
            <span className = "pageLink" id="contact"> </span>
            <PageTitle title="Contact"/>
            <SectionPaper>
                <SectionDescription description="I would love to hear from you. The best way to reach me is email or linkedin."/>

                {
                    contactInfo.map( contactType =>
                        <Button
                            key={contactType.type}
                            variant="outlined"
                            color="primary"
                            size="large"
                            className={classes.contactMeButton}
                            startIcon={contactType.icon}
                            onClick={(event) => window.open(contactType.link)}
                        >
                            {
                              contactType.value.length > 10 ?
                                <div style={{fontSize: 12}}> {contactType.value}</div>
                              :
                                <div> {contactType.value} </div>}
                        </Button>
                    )
                }

            </SectionPaper>
        </div>
    )
}