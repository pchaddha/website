import React from "react";
import cx from 'clsx';

import { makeStyles } from '@material-ui/core/styles';
import {Card, CardMedia, CardContent, Chip, Typography } from '@material-ui/core';
import Link from '@material-ui/core/Button';

import { useBlogTextInfoContentStyles } from '@mui-treasury/styles/textInfoContent/blog';
import { useOverShadowStyles } from '@mui-treasury/styles/shadow/over';

import {PageTitle} from "../../utilities/Components";

const useStyles = makeStyles(({ breakpoints, spacing }) => ({
  root: {
    margin: 'auto',
    borderRadius: spacing(2), // 16px
    transition: '0.3s',
    boxShadow: '0px 14px 80px rgba(34, 35, 58, 0.2)',
    position: 'relative',
    marginLeft: 'auto',
    overflow: 'initial',
    background: '#ffffff',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: spacing(2),
    [breakpoints.up('md')]: {
      flexDirection: 'row',
      paddingTop: spacing(2),
      width: '50%',
    },
    [breakpoints.down('sm')]: {
      width: '100%',
    }
  },
  media: {
    width: '88%',
    marginLeft: 'auto',
    marginRight: 'auto',
    [breakpoints.up('md')]: {
      marginTop: spacing(-3),
    },
    [breakpoints.down('sm')]: {
      marginTop: spacing(1),
    },
    height: 0,
    paddingBottom: '48%',
    borderRadius: spacing(2),
    backgroundColor: 'white',
    position: 'relative',
    border: "1px solid purple",
    [breakpoints.up('md')]: {
      width: '100%',
      marginLeft: spacing(-3),
      marginTop: 0,
      transform: 'translateX(-8px)',
    },
    '&:after': {
      content: '" "',
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: spacing(2), // 16
      opacity: 0.5,
    },
  },
  content: {
    padding: 24,
  },
  cta: {
    marginTop: 24,
    textTransform: 'initial',
  },
}));

export const BlogCardDemo = React.memo(function BlogCard({courses}) {
  const styles = useStyles();
  const {
    button: buttonStyles,
    ...contentStyles
  } = useBlogTextInfoContentStyles();
  const shadowStyles = useOverShadowStyles();
  return (
    <Card className={cx(styles.root, shadowStyles.root)}>
      <CardMedia
        className={styles.media}
        image={'static/experience/university_of_waterloo_logo.png'}
      />
      <CardContent>
        <Typography variant="h6" component="h6">
          BASc. Nanotechnology Engineering; Psychology Minor
        </Typography>
        <Typography color="primary" variant="subtitle2" component="subtitle2">
          September, 2016 TO April, 2022
        </Typography>
        <Typography gutterBottom variant="p" component="p">
          - Multidisplinary program with focus on micro- and nano-scale systems. <br/>
          - (September, 2021 to Present) Teaching Assistant: NE 121- Chemical Principles.

        </Typography>
        {
          courses.map( courseName => 
            <Chip size="small" color="primary" key={courseName} label={courseName} variant="outlined"/>
          )
        }
        <Link
          component="chip"
          href="https://uwaterloo.ca/future-students/programs/nanotechnology-engineering"
          rel="noreferer"
          target="_blank"
          color="primary"
          variant="outlined"
          size="small"
          style={{width: "100%", position: "relative", bottom: "-2vh"}}
        >
          Click here to learn more
        </Link>
      </CardContent>
    </Card>
  );
});

export default function Education () {

    const courses = [
      "Photonic Material and Devices",
      "Quantum Mechanics",
      "Micro And Nanosystems Design",
      "Optimization Methods",
    ]

    return (
        <div className="page">
            <span className = "pageLink" id="education"> </span>
            <PageTitle title={"Education"}/>
            <BlogCardDemo courses={courses} />
        </div>
    )
}