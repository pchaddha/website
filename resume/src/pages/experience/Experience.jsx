import React, {useState, useEffect} from "react";

import {makeStyles} from "@material-ui/core/styles";
import { GridList, GridListTile } from "@material-ui/core";

import {PageTitle} from "../../utilities/Components";
import {useWidth} from "../../utilities/ScreenSizes";

import {ExpCard} from "./ExperienceCard";

const experiences = [
    {
        expName: "tesla_2",
        header: {
            "company": "tesla",
            "avatar": "./static/experience/tesla_logo.png",
            "title": "Data Engineering, Cell Engineering",
            "date": "June 20, 2022 to Current",
        },
        media: {
            "image": "/static/experience/nikon.png",
            "title": "Model S Chassis",
        },
        roleDescription: [
            "Lead of the computed tomography imaging data pipeline and related front-end applications, critical for production machine calibration operations, contamination detection, and various research and development activities.",
            "Scaled imaging pipeline by a factor of 10x, while stabilizing imaging pipeline P99 time from 120 hours to 16 hours and decreased mean processing time from 6 hours to 1.2 hours through queue distribution (Celery), automatic scaling (K8), creating alerts (Splunk) and implementing state machines (MySQL).",
            "Led investigation of image pipeline data retention costs. Wrote data retention policies and software to implement those policies. Deleted intermediary data and moved archive image data to glacial tier in S3. This led to immediate bottom-line savings of $200K per quarter, scaling to $5 million dollars over the upcoming 5 years, and $2 million dollars every subsequent year.",
            "Implemented pagination and virtualization on front end applications reducing data load time by a factor of 10x and the average payload size by 100x.",
        ],
        tags: [
            "Python",
            "TypeScript",
            "React.js",
            "SQLAlchemy",
            "Pytest",
            "Poetry",
            "Cypress",
            "Splunk",
            "Docker",
            "Jenkins",
            "Celery",
            "Redis",
            "MySQL",
            "AWS S3",
            "Graph API",
        ],
    },
    {
        expName: "lumentum_2",
        header: {
            "company": "lumentum",
            "avatar": "./static/experience/lumentum_logo.png",
            "title": " Optics / Design Authority Intern",
            "date": "Jan 1, 2021 to Aug 15, 2021",
        },
        media: {
            "image": "/static/experience/lumentum_wss.jpg",
            "title": "Wavelength Selective Switch",
        },
        roleDescription: [
            "Led technical discussions and negotiations with customers on multi-million-dollar optical product lines.",
            "Fully created, tested, and documented 3 full-stack, data-engineering apps halving yield analysis time.",
            "Increased speed of product kitting algorithm by 1000% by optimizing database connections, adapting a tree-based structure, and refactoring append/storage commands.",
            "Implemented Git Flow and virtualized the work environment using Poetry to ensure that the various builds of the package were source-controlled, tested, and met requirements for three separate teams.",
            "Increased task turnover speed by over 20% by onboarding team onto Jira.",
        ],
        tags: [
            "Agile",
            "Jira",
            "MySQL",
            "Python",
            "Poetry",
            "Plotly Dash",
            "Jenkins",
        ],
    },
    {
        expName: "tesla_1",
        header: {
            "company": "tesla",
            "avatar": "./static/experience/tesla_logo.png",
            "title": "Cell Engineering Intern",
            "date": "Jan 1, 2020 to Dec 18, 2020",
        },
        media: {
            "image": "/static/experience/tesla_model_s.jpg",
            "title": "Model S Chassis",
        },
        roleDescription: [
            "Consolidated cell data from 6 labs located throughout the United States and Canada into a central database through the design of a web application using REACT.js, Python, REST, and MySQL.",
            "Independently designed and implemented the primary cell tracker and informatic system used by all cell engineers, technicians and contractors allowing for instant cell and cycler lookup, and data approval.",
            "Wrote and tested critical real-time temperature management scripts to avert catastrophic thermal events.",
            "Hosted biweekly design reviews to provide updates and receive feedback from all consumers of cell data.",
        ],
        tags: [
            "Python",
            "Docker",   
            "Pytest",
            "React",
            "JavaScript",
            "Node",
            "SQL Alchemy",
        ],
    },
    {
        expName: "lumentum_1",
        header: {
            "company": "lumentum",
            "avatar": "./static/experience/lumentum_logo.png",
            "title": "Algorithm and Analytics Intern",
            "date": "Sep 1, 2018 to Apr 18, 2019",
        },
        media: {
            "image": "/static/experience/lumentum_wss.jpg",
            "title": "Wavelength Selective Switch",
        },
        roleDescription: [
            "Worked on decreasing the production build time per WSS device resulting in company savings over $40 000 per month and increased WSS product throughput by 1%.",
            "Analyzed millions of lines of test data from Wavelength Selective Switches to monitor and enhance performance through noise removal, and various regression techniques.",
            "Calibrated and tested WSS device performance under various temperature and calibration conditions.",
        ],
        tags: [
            "Python",
            "Pandas",   
        ],
    },
    {
        expName: "universityOfCalgary_1",
        header: {
            "company": "university of calgary",
            "avatar": "./static/experience/university_of_calgary_logo.jpeg",
            "title": "CERC Research Student",
            "date": "Jan 1, 2018 to Apr 30, 2018",
        },
        media: {
            "image": "/static/experience/university_of_calgary_mst.png",
            "title": "Micronized Sulfur Technology",
        },
        roleDescription: [
            "Researched slow-release fertilizer composition and properties to maximize nitrogen content, depress freezing point, decrease rate of release, and optimize cost.",
            "Created formulation that lowered freezing point by 5C and increased nitrogen content by 30% leading to pilot plant adaption, and subsequent full-scale product line.",
        ],
        tags: [
            "Matlab",
            "VBA Excel",
        ],
    },
]
const expCardStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
    },
    gridList: {
      [theme.breakpoints.up('sm')] : {
        flexWrap: 'nowrap',
      },
      [theme.breakpoints.down('sm')] : {
        maxWidth: '100vw',
        marginBottom: theme.spacing(3),
      },
      
      transform: 'translateZ(0)',
    },
}));

function ExperienceCards({width}) {
  const [numCols, setNumCols] = useState(1);
  const classes = expCardStyles();

  useEffect( () => {
    if (width === 'sm' | width === 'xs') {
        setNumCols(1);
    } else if (width === 'md') {
        setNumCols(2.5);
    } else {
        setNumCols(3.5);
    }
  }, [width])

  return (
    <div className={classes.root}>
        <GridList cellHeight={625} className={classes.gridList} cols={numCols}>
            {experiences.map((experience, index) => {
                return (
                    <GridListTile key={experience.expName} className={classes.gridListTile}>
                        <ExpCard 
                            experience={experience}
                            key={`${experience.header.company}_${index}`}
                        />
                    </GridListTile>
                )
            })}
        </GridList>
    </div>
  );
}

export default function Experience () {
    const width = useWidth();
    return (
        <div className="page">
            <span className = "pageLink" id="experience"> </span>
            <PageTitle title={"Experience"}/>
            <ExperienceCards width={width}/>
        </div>
    )
}