import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import {Card, CardHeader, CardMedia, CardContent, Avatar, Typography} from '@material-ui/core';
import {List, ListItem, ListItemText} from "@material-ui/core";

import {allSkills} from "../skills/Skills";
import {SkillChip} from "../skills/SkillsComponents";

const useStyles = makeStyles((theme) => {
  return ({
    root: {
      marginLeft: 10,
      marginRight: 10,
      marginBottom: theme.spacing(1),
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        marginBottom: theme.spacing(3),
      },
      height: 800,
      borderRadius: 20
    },
    chipArea: {
      height: 100,
    },
    expandedHeight: {
      height: 625,
      scroll: "y",
    },
    media: {
      height: 0,
      paddingTop: '25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    content: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(1),
        borderRadius: 20,
    },
    list: {
      height: 300,
      overflowY: 'scroll !important',
      scroll: "visible"
    }
  })
});

export function ExpCard({experience}) {
    const classes = useStyles();
    const {header, media, roleDescription, tags} = experience;
    const skills = tags.map( tagName => allSkills.find( skill => skill.name === tagName));

    return (
    <div style={{display: "inline-block"}}>
        <Card
          className={classes.root}
          >

            <CardHeader
                avatar={<Avatar aria-label={header.company} className={classes.avatar} title={header.company.toUpperCase()} alt={header.company} src={header.avatar}/>}
                // action={<IconButton aria-label="settings"> <MoreVertIcon /> </IconButton>}
                title={header.title}
                subheader={header.date}
            />

            <CardMedia className={classes.media} image={media.image} title={media.title}/>
            
            <CardContent className={classes.chipArea}>
                {skills.map( skill => {
                  return (
                    skill === undefined ?
                      null
                    :
                      <SkillChip key={`experience_${skill.name}`} skill={skill}/>
                  )
                }
                )}           
            </CardContent>

            <CardContent className={classes.content}>
                <Typography variant="body2" component="p" style={{textAlign:"center"}}> Roles and Responsibilites </Typography>
                <List className={classes.list} dense={true}>
                    {roleDescription.map((role, roleIndex) =>
                        <ListItem key={roleIndex}>
                            <ListItemText primary={`-${role}`}/>
                        </ListItem>
                    )}
                </List>
            </CardContent>

        </Card>
    </div>
    )
}
