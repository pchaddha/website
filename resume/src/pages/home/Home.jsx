import React from "react";

import Typography from '@material-ui/core/Typography';
import RootRef from '@material-ui/core/RootRef';
import {Grid, makeStyles} from "@material-ui/core";

import {useStyle} from "../../assets/Styles";

import {SectionTitle, SectionDescription, SectionPaper} from "../../utilities/Components";

const ResumeTitle = () => {
    const classes = useStyle();
    return (
        <Typography className={classes.resumeTitle} variant="h2" component="h2">
            Parmandeep Chaddha
        </Typography>
    )
}

const aboutMeDescriptionText = `I am a learner with a passion for solving tough problems.
I currently live in the San Fransisco / Bay Area, where I spend most weekends either hiking, reading, or skiing.
I enjoy working on tough problems and coming up with creative solutions to solve them. Sometimes my solutions don't work.`

const AboutMe = () => {
    return (
        <SectionPaper style={{display: "inline-block"}} width={'98%'} margin={1}>
            <SectionTitle title="About Me"/>
            <SectionDescription description={aboutMeDescriptionText}/>
        </SectionPaper>
    )
}

const profilePicStyles = makeStyles( theme => ({
    profilePic: {
        margin: '1%',
        borderRadius: 20,
        [theme.breakpoints.down('sm')]: {
            width: '98%',
        },
        [theme.breakpoints.up('sm')]: {
            height: props => props.height,
        },
        [theme.breakpoints.up('md')]: {
            height: props => props.height,
        },
        float: "right",
    },
}));


const ProfilePic = ({height}) => {
    const classes = profilePicStyles({height: height});

    return (
        <img className = {classes.profilePic} alt="Parmandeep Chaddha" src="/static/home/profile.jpg" />
    )
}

export default function Home() {

    const aboutMeRef = React.useRef(null);
    const [profilePicHeight, setProfilePicHeight] = React.useState(0);

    function recalculateHeight() {
        if (aboutMeRef.current) {
            setProfilePicHeight(aboutMeRef.current.clientHeight);
        };
    }
    React.useEffect( () => {
        setTimeout( () => recalculateHeight(), 200); // This is essential to render the image correctly.
        window.addEventListener('resize', recalculateHeight);
    }, []);

    return (
        <div className="page">
            <span className = "pageLink" id="home"> </span>
            <ResumeTitle />
            <Grid container>
                <Grid item lg={2} md={2} xs={false}> </Grid>
                <Grid item lg={2} md={4} sm={4} xs={12}> <ProfilePic height={profilePicHeight}/> </Grid>
                <Grid item lg={6} md={5} sm={8} xs={12}>
                    <RootRef rootRef={aboutMeRef}>
                        <AboutMe/>
                    </RootRef>
                </Grid>
                <Grid item md={2} xs={12}> </Grid>
            </Grid>
        </div>
    )
}