import React from "react";
import { ButtonGroup, Button} from "@material-ui/core";

import {useStyle} from "../../assets/Styles";
import {SITE_COLORS} from "../../assets/ThemeProvider";

function Navbar () {

    const classes = useStyle();

    const pages = [
        "home",
        "skills",
        'experience',
        'education',
        'projects',
        'contact',
    ];

    return (
        <div className={classes.navbarContainer}>
        <ButtonGroup 
            className={classes.navbar}
            variant="text"
            color="primary"
            aria-label="text primary button group"
        >
            {pages.map( (pageName) => 
                <Button key={pageName} color='primary' variant='outlined'> 
                    <a style={{textDecoration: 0, color: SITE_COLORS.purple}}href={`#${pageName}`}>
                        {pageName[0].toUpperCase() + pageName.slice(1)}
                    </a>
                </Button>
            )}
        </ButtonGroup>
        </div>
    )
}

export default Navbar