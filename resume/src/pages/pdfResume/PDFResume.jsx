import React from "react";
import Button from "@material-ui/core/Button/";
import { makeStyles } from "@material-ui/core/styles";

const pdfResumeStyles = makeStyles(theme => ({
    pdfResumeButton: {
        borderRadius: 20,
        float: 'right',
        position: 'fixed',
        [theme.breakpoints.up('md')]: {
            top: theme.spacing(3),
            zIndex: 100000000000000000000,
            right: theme.spacing(3),
        },
        [theme.breakpoints.down('sm')]: {
            bottom: theme.spacing(1),
            zIndex: 100000000000000000000,
            right: theme.spacing(1),
        }
    },
}));
export default function PDFResume () {
    const classes = pdfResumeStyles();
    return (
        <Button
            variant="contained"
            color="primary"
            className={classes.pdfResumeButton}
            onClick={() => window.open("/static/resume.pdf")}
        >
            {"Resume (PDF)"}    
        </Button>
    )
} 