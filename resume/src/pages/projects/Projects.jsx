import React from "react";

import {Card, CardHeader, CardMedia, CardContent, Chip, Typography, Divider} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import GitHubIcon from '@material-ui/icons/GitHub';

import {PageTitle} from "../../utilities/Components";
import {SkillChip} from "../skills/SkillsComponents";
import {allSkills} from "../skills/Skills";

const useProjectCardStyles = makeStyles( theme => ({
    root: {
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
        [theme.breakpoints.up('md')]: {
            width: 300,
        },
        display: "inline-block",
        borderRadius: 10,
        padding: 10,
        marginLeft: 10,
    },
    header: {
        textAlign: "center",
    },
    media: {
        borderRadius: 10,
        height: 140,
        border: '1px solid purple',
    },
    bitBucketChip: {
        justifyContent: 'center',
        position: "relative",
        display: 'flex',
        margin: 10,
        top: 10,
    },
    projectContainer: {
        display: "block",
        [theme.breakpoints.down('sm')]: {
        },
        [theme.breakpoints.up('md')]: {
            width: 950,
            margin: "0px auto",
        },
    }
}));

const projects = [
    {
        name: "Find PS5",
        image: "/static/projects/2021_ps5.jpg",
        imageTitle: "Sony Playstation 5",
        imageSubtitle: "SONY PLAYSTATION 5",
        chipLink: "https://bitbucket.org/pchaddha/",
        chipDescription: "Bitbucket",
        languageTags: ["Python", "Pytest"],
        description: "Scripts to monitor the twitter feed of EB Games Canada (GameStop) and Best Buy Canada for alerts on PS5 availibility.",
    },
    {
        name: "Poems by Chaddha",
        image: "/static/projects/poems_by_chaddha.png",
        imageTitle: "Poems by Chaddha",
        imageSubtitle: "POEMS BY CHADDHA",
        chipDescription: "Bitbucket",
        chipLink: "https://bitbucket.org/pchaddha/poems_by_chaddha/",
        languageTags: ["React.js", "JavaScript"],
        description: "Website showcasing poetry written by my sister and myself. The end goal is to have a space where everyone can share their poems."

    },
    {
        name: "Terminal Fall 2021",
        image: "/static/projects/terminal_flyer.png",
        imageTitle: "Terminal Flyer",
        imageSubtitle: "TERMINAL FLYER",
        chipLink: "/static/projects/terminal_certificate.pdf",
        chipDescription: "Competition Certificate",
        languageTags: ["Python"],
        description: `An AI programming competition where teams code algorithms for a strategy game, and compete head-to-head in a tournament.`

    }
]

function ProjectCard ({project}) {
    const projectClasses = useProjectCardStyles();
    const {name, image, imageTitle, imageSubtitle, chipLink, chipDescription, languageTags, description} = project;
    const skills = languageTags.map( tagName => allSkills.find( skill => skill.name === tagName));

    return (
        <Card className={projectClasses.root}>
            <CardMedia
                image={image}
                title={imageTitle}
                className={projectClasses.media}
            />

            <CardHeader title={name} className={projectClasses.header}/>

            <Divider color="primary" style={{width: '30%', marginLeft: '35%', marginRight: "35%"}} variant="fullWidth"/>

            <CardContent className={projectClasses.content}>

                {skills.map( skill => 
                    <SkillChip key={`project_${skill.name}`} skill={skill}/>
                )}

                <Typography component="p" variant="body2">
                    {description}
                </Typography>
                
                { chipLink !== null ?
                    <Chip
                    className={projectClasses.bitBucketChip}
                    icon={<GitHubIcon/>}
                    label={chipDescription}
                    color="primary"
                    onClick={()=>window.open(chipLink)}
                    />
                    :
                    <> </>
                }

            </CardContent>


        </Card>
    )
}


export default function Projects() {
    const projectClasses = useProjectCardStyles();
    return (
        <div className="page">
            <span className = "pageLink" id="projects"> </span>
            <PageTitle title={"Projects"} />
            <div className={projectClasses.projectContainer}>
                {projects.map( project =>
                    <ProjectCard project={project} key={`project_card_${project.name}`}/>
                )}
            </div>
        </div>
    )
}