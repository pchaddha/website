import React from "react";

import Divider from '@material-ui/core/Divider';

import {PageTitle, SectionPaper} from "../../utilities/Components";
import {SkillTitle, SkillChip} from "./SkillsComponents";
import skillsData from "./technical_skills.json";

export const allSkills = skillsData;



const FrameWorks = () => {
  const framework_skills = skillsData.filter(
    (value) => value?.category === "frameworks"
  );
  
  return (
    <div style={{display: 'inline-block'}}>
      <SkillTitle title="Frameworks"/>
      {framework_skills.map((skill) =>
        <SkillChip key={`framework_${skill?.name}`} skill={skill}/>
      )}
    </div>
  )
}


const SoftSkills = () => {
  const projectManagement = skillsData.filter(
    (value) => value?.category === "project_management"
  );
  
  return (
    <div style={{display: 'inline-block'}}>
      <SkillTitle title="Project Management"/>
      {projectManagement.map( skill =>
        <SkillChip key={`soft_${skill.name}`} skill={skill} />
      )}
    </div>
  )
}

const Languages = () => {
  const languages = skillsData.filter(
    (value) => value?.category === "language"
  );
  
  return (
    <div style={{display: 'inline-block'}}>
      <SkillTitle title="Languages"/>
      {languages.map( skill =>
        <SkillChip key={`language_${skill.name}`} skill={skill} />
      )}
    </div>
  )
}


const MonitoringAndDeployment = () => {
  const languages = skillsData.filter(
    (value) => value?.category === "monitoring_and_deployment"
  );
  
  return (
    <div style={{display: 'inline-block'}}>
      <SkillTitle title="Monitoring and Deployment"/>
      {languages.map( skill =>
        <SkillChip key={`md_${skill.name}`} skill={skill} />
      )}
    </div>
  )
}

const DataManagement = () => {
  const languages = skillsData.filter(
    (value) => value?.category === "data_management"
  );
  
  return (
    <div style={{display: 'inline-block'}}>
      <SkillTitle title="Data Management"/>
      {languages.map( skill =>
        <SkillChip key={`data_${skill.name}`} skill={skill} />
      )}
    </div>
  )
}


export default function Skills () {

  return (
    <div className="page">
    <span className = "pageLink" id="skills"> </span>
    <PageTitle title="Skills"/>
  
    <SectionPaper center={true}>
      <Languages/>

      <Divider style={{backgroundColor: '#92a9bb', margin: '2vh'}} variant="middle"/>

      <MonitoringAndDeployment/>

      <Divider style={{backgroundColor: '#92a9bb', margin: '2vh'}} variant="middle"/>

      <DataManagement/>

      <Divider style={{backgroundColor: '#92a9bb', margin: '1vh'}} variant="middle"/>

      <SoftSkills/>

      <Divider style={{backgroundColor: '#92a9bb', margin: '2vh'}} variant="middle"/>

      <FrameWorks/>

    </SectionPaper>
    </div>
  )
}