import React from 'react';
import { Typography, Chip, Avatar} from '@material-ui/core';

export const SkillTitle = ({title}) => {
  return (
    <Typography variant='subtitle1' component='h5' > {title} </Typography>
  )
}

export function SkillChip ({skill}) {
  const imageURL = `/static/skills/${skill.image_url}`;
  return (
    <Chip
      key={skill.name}
      color="primary"
      variant="outlined"
      size="medium"
      avatar={<Avatar alt={skill.name} src={imageURL} title={skill.imageTitle}/>}
      label={skill.name}
      clickable
    />
  )
}
