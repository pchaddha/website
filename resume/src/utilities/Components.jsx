import React from "react";
import {Paper, Typography, Card, CardContent, CardMedia, CardActionArea, Chip, FormControl, FormControlLabel, Checkbox} from "@material-ui/core";
import {useStyle} from "../assets/Styles";
import { makeStyles } from "@material-ui/core/styles";

export function PageTitle({title}) {
    const classes = useStyle()
    return (
        <>
            <Typography className={classes.title} variant="h3" component="h2">
              {title}
            </Typography>
        </>
    )
}

const useSectionStyles = (width, margin) => makeStyles( theme => ({
  sectionPaper: {
      borderRadius: '20px',
      padding: theme.spacing(2),
      [theme.breakpoints.up('md')]: {
          width: width ? width : "50%",
          margin: margin ? margin : `2vh 25%`
      },
      [theme.breakpoints.down('sm')]: {
          width: '100%',
          margin: '2vh 1%',
      },
  },
}));
export function SectionPaper(props) {
  const classes = useSectionStyles(props.width, props.margin)();
  return (
    <Paper elevation={5} className={classes.sectionPaper}>
      {props.children}
    </Paper>

  )
 
}

export function SectionTitle({title}) {
    const classes = useStyle()
    return (
        <Typography className={classes.title} variant="h4" component="h3">
          {title}
        </Typography>
    )
}

export function SectionDescription({description}) {
    return (
      <Typography variant="body1" component="p">
        {description}
      </Typography>
    )
}

export function SkillCard({imageTitle, cardTitle, imageURL, cardTags, passedAssessment, summary}) {
  const classes = useStyle();

  return (
    <Card className={classes.cardRoot}>
      <CardActionArea>
        <CardMedia
          className={classes.cardMedia}
          image={imageURL}
          title={imageTitle}
        />
  
        <CardContent className={classes.cardContent}>
          <Typography variant="h6" component="h6">
            {cardTitle}
          </Typography>

          <Typography variant="p" component="p">
            <p style={{textAlign: "left", margin: "0px 2px"}}> 
              {summary}
            </p>
          </Typography>
          
          <FormControl className={classes.cardSummary}>
            <FormControlLabel 
              control={
                <Checkbox color="secondary" checked={passedAssessment} name={`passed_${cardTitle}`}/>
              }
              label={
                <Typography className={classes.cardLabel} variant="p"> LinkedIn Assessment</Typography>
              }
              labelPlacement="start"
            />
          </FormControl>

          {
            cardTags.map( (value, index) => {
              return (
                <Chip className={classes.cardChip} elevation={2} size="small" key={index} label={value} disabled/>
              )
            })
          }
        </CardContent>
      </CardActionArea>


    </Card>
  );    
}
